module Advertisements
  class Search
    def call(params)
      scope = Advertisement.unscoped
      scope = apply_category_filter(scope, params)
      scope = apply_sorting(scope, params)

      Success(scope)
    end

    private

    def apply_category_filter(scope, params)
      return scope unless params[:category]

      scope.where(category: params[:category])
    end

    def apply_sorting(scope, params)
      return scope unless params[:sort_by]

      sort_direction = params[:sort_direction] || 'asc'

      case params[:sort_by]
      when 'created_at', 'price'
        scope.order(params[:sort_by] => sort_direction)
      else
        scope
      end
    end
  end
end
