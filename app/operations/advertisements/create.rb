module Advertisements
  class Create
    include Dry::Monads::Do.for(:call)

    include AdsBoard::Deps[
      validate: 'contracts.advertisements.new',
      generate_uid: 'services.generate_uid'
    ]

    def call(current_user, params)
      attributes = yield validate.(params)

      advertisement = Advertisement.create!(
        user: current_user,
        uid: generate_uid.(),
        **attributes.except(:photos)
      )

      advertisement.images.attach(attributes[:photos]) if attributes[:photos]

      Success(advertisement)
    end
  end
end
