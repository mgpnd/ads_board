module Users
  class Create
    include Dry::Monads::Do.for(:call)

    include AdsBoard::Deps[
      validate: 'contracts.users.new'
    ]

    def call(params)
      attributes = yield validate.(params)

      user = User.create!(attributes)

      Success(user)
    end
  end
end
