module Users
  class Authenticate
    def call(email, password)
      user = User.find_by(email: email)
      return Failure(:not_authenticated) if user.nil?

      if user.authenticate(password)
        Success(user)
      else
        Failure(:not_authenticated)
      end
    end
  end
end
