import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import {
  AppBar,
  Button,
  Toolbar,
  Typography
} from '@material-ui/core'
import { AccountCircle } from '@material-ui/icons';

const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  accountIcon: {
    marginLeft: theme.spacing(2),
  },
  username: {
    marginLeft: theme.spacing(1),
  },
}));

export default function Header(props) {
  const classes = useStyles();

  return(
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h6" className={classes.title}>
          AdsBoard
        </Typography>

        <div className={classes.grow}></div>

        {
          props.currentUser && (
            <Button color="inherit" onClick={props.onCreateAdvertisementClick}>
              Create Advertisement
            </Button>
          )
        }

        {
          props.currentUser ?
          (
            <React.Fragment>
              <AccountCircle className={classes.accountIcon} />
              <Typography varian="h6" className={classes.username}>{props.currentUser.id}</Typography>
            </React.Fragment>
          ) :
          (
            <React.Fragment>
              <Button color="inherit" onClick={props.onLogInClick}>Login</Button>
              <Button color="inherit" onClick={props.onSignUpClick}>Sign Up</Button>
            </React.Fragment>
          )
        }
      </Toolbar>
    </AppBar>
  )
}
