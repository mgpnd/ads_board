import React from 'react';
import { useState } from 'react';
import {
  Button,
  DialogActions,
  Dialog,
  DialogTitle,
  DialogContent,
  TextField,
} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';

export default function LogInDialog(props) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [invalidCredentials, setInvalidCredentials] = useState(false);

  const handleLogIn = () => {
    const postData = { email, password };

    fetch('/api/v1/sessions', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(postData),
    })
    .then(response => {
      if (response.status === 201) {
        response.json().then(body => props.onLogIn(body.data));
      } else {
        setInvalidCredentials(true);
      }
    })
  };

  return (
    <Dialog open={props.open} onClose={props.onClose}>
      <DialogTitle>Log In</DialogTitle>
      <DialogContent>
        { invalidCredentials &&
          <Alert severity="error">Invalid email or password</Alert>
        }
        <form>
          <div>
            <TextField
              id="email"
              label="Email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div>
            <TextField
              id="password"
              type="password"
              label="Password"
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
        </form>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleLogIn} color="primary">Log In</Button>
      </DialogActions>
    </Dialog>
  )
}
