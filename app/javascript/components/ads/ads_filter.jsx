import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import { FormControl, Grid, InputLabel, MenuItem, Select } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  filters: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
  },
  formControl: {
    minWidth: 120,
  },
}))

export default function AdsFilter(props) {
  const classes = useStyles();

  return(
    <Grid
      container
      direction="row"
      justify="space-between"
      alignItems="flex-start"
      className={classes.filters}
    >
      <FormControl className={classes.formControl} >
        <InputLabel id="category-label">Category</InputLabel>
        <Select
          labelId="category-label"
          id="category-select"
          value={props.category}
          onChange={props.onCategoryChange}
        >
          <MenuItem value="all">All</MenuItem>
          <MenuItem value="cats">Cats</MenuItem>
          <MenuItem value="dogs">Dogs</MenuItem>
          <MenuItem value="hamsters">Hamsters</MenuItem>
          <MenuItem value="snakes">Snakes</MenuItem>
        </Select>
      </FormControl>
      <FormControl className={classes.formControl} >
        <InputLabel id="sort-label">Sort By</InputLabel>
        <Select
          labelId="sort-label"
          id="sort-select"
          value={props.sortBy}
          onChange={props.onSortByChange}
        >
          <MenuItem value="created_at_asc">Publication Date (from oldes)</MenuItem>
          <MenuItem value="created_at_desc">Publication Date (from newest)</MenuItem>
          <MenuItem value="price_asc">Price (ascending)</MenuItem>
          <MenuItem value="price_desc">Price (descending)</MenuItem>
        </Select>
      </FormControl>
    </Grid>
  )
}
