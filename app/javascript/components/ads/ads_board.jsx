import React from 'react';
import { useEffect, useState } from 'react';
import qs from 'qs';
import { makeStyles } from '@material-ui/core/styles';
import Alert from '@material-ui/lab/Alert';
import { LinearProgress } from '@material-ui/core';

import AdsGrid from './ads_grid';
import AdsFilter from './ads_filter';

const useStyles = makeStyles((theme) => ({
  content: {
    marginTop: theme.spacing(2),
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
  },
}));

const buildSortParam = (sortBy) => {
  const [attr, direction] = sortBy.split(/_([a-zA-Z]+)$/)

  return {
    sort_by: attr,
    sort_direction: direction,
  }
}

export default function AdsBoard(props) {
  const classes = useStyles();
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [ads, setAds] = useState([]);
  const [category, setCategory] = useState("all");
  const [sortBy, setSortBy] = useState("created_at_desc");

  useEffect(() => {
    const params = buildSortParam(sortBy);

    if (category != "all") {
      params['category'] = category
    }

    const url = '/api/v1/advertisements'
    const fullUrl = `${url}?${qs.stringify(params)}`

    fetch(fullUrl)
    .then(response => response.json())
    .then(
      (result) => {
        setIsLoaded(true);
        setAds(result.data);
        props.onAdsLoaded();
      },
      (error) => {
        setIsLoaded(true);
        setError(error);
      }
    );
  }, [category, sortBy, props.hasLastAds]);

  return(
    <div className={classes.content}>
      {
        isLoaded ? (
          error ?
          (<Alert severity="error"> {error} </Alert>) :
          (
            <React.Fragment>
              <AdsFilter
                category={category}
                onCategoryChange={(e) => setCategory(e.target.value)}
                sortBy={sortBy}
                onSortByChange={(e) => setSortBy(e.target.value)}
              />
              <AdsGrid ads={ads} />
            </React.Fragment>
          )
        ) :
        (<LinearProgress />)
      }
    </div>
  )
}
