import React from 'react';
import {
  GridList,
  GridListTile,
} from '@material-ui/core';

import AdItem from './ad_item';

export default function AdsGrid(props) {
  return(
    <GridList cols={3} cellHeight="auto">
      {
        props.ads.map(item => (
          <GridListTile key={item.id}>
            <AdItem item={item} />
          </GridListTile>
        ))
      }
    </GridList>
  )
}
