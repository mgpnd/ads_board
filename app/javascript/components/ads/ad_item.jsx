import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AwesomeSlider from 'react-awesome-slider';
import 'react-awesome-slider/dist/styles.css';
import placeholder from '../../images/noimageavailable.png';

import {
  Card,
  CardContent,
  CardHeader,
  CardMedia,
  Typography
} from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  gridItem: {
    margin: theme.spacing(2)
  },
  price: {
    fontWeight: 'bold',
    marginTop: theme.spacing(1),
  }
}))

export default function AdItem(props) {
  const classes = useStyles();

  const images = props.item.attributes.photos;

  return (
    <Card className={classes.gridItem}>
      <CardHeader
        title={props.item.attributes.title}
        subheader={props.item.attributes.category}
      />
      <CardMedia>
        <AwesomeSlider bullets={false} infinite={false}>
          {
            images.length > 0 ?
            (images.map(img => (<div key={img} data-src={img}/>))) :
            (<div data-src={placeholder} />)
          }
        </AwesomeSlider>
      </CardMedia>
      <CardContent>
        <Typography variant="body1">
          {props.item.attributes.description}
        </Typography>
        <Typography className={classes.price} variant="body1">
          ${props.item.attributes.price}
        </Typography>
        {
          props.item.attributes.phone &&
          (
            <Typography variant="body2">{props.item.attributes.phone}</Typography>
          )
        }
        {
          props.item.attributes.address &&
          (
            <Typography variant="body2">{props.item.attributes.address}</Typography>
          )
        }
      </CardContent>
    </Card>
  );
}
