import React from 'react';
import { useState } from 'react';
import InputMask from "react-input-mask";
import { makeStyles } from '@material-ui/core/styles';
import {
  Button,
  DialogActions,
  Dialog,
  DialogTitle,
  DialogContent,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Grid,
  Typography,
} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';

const useStyles = makeStyles((theme) => ({
  wide: {
    width: '100%',
  },
  row: {
    marginBottom: theme.spacing(4),
  },
  formControl: {
    minWidth: 180,
    marginBottom: theme.spacing(2),
    marginRight: theme.spacing(4),
  },
  photos: {
    marginTop: theme.spacing(4),
  },
  photoUploader: {
    marginTop: theme.spacing(2),
  },
}));

const buildFormData = (attributes) => {
  const formData = new FormData();
  formData.append('title', attributes.title);
  formData.append('category', attributes.category);
  formData.append('description', attributes.description);
  formData.append('price', attributes.price);

  if (attributes.address && attributes.address.length > 0) {
    formData.append('address', attributes.address);
  }

  if (attributes.phone && attributes.phone.length > 0) {
    formData.append('phone', attributes.phone);
  }

  attributes.photos.forEach(file => formData.append('photos[]', file));

  return formData;
}

export default function CreateAdvertisementDialog(props) {
  const classes = useStyles();

  const [errors, setErrors] = useState({});

  const [title, setTitle] = useState('');
  const [category, setCategory] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [address, setAddress] = useState(props.currentUser ? props.currentUser.attributes.address || '' : '');
  const [phone, setPhone] = useState(props.currentUser ? props.currentUser.attributes.phone || '' : '');
  const [photoInputs, setPhotoInputs] = useState([]);
  const [photoValues, setPhotoValues] = useState([]);

  const resetForm = () => {
    setTitle('');
    setCategory('');
    setDescription('');
    setPrice('');
    setAddress('');
    setPhone('');
  }

  const handleClose = () => {
    resetForm();
    props.onClose();
  }

  const addPhotoInput = () => {
    const index = photoInputs.length;

    setPhotoInputs([
      ...photoInputs,
      (<PhotoUploader key={index} onChange={updatePhotoValue(index)} />)
    ]);

    setPhotoValues([...photoValues, null]);
  }

  const updatePhotoValue = index => {
    return (e) => {
      setPhotoValues([...photoValues.slice(0, index), e.target.files[0], ...photoValues.slice(index)]);
    };
  }

  const handleSubmit = () => {
    const postData = buildFormData({
      title,
      category,
      description,
      price,
      address,
      phone,
      photos: photoValues
    });

    fetch('/api/v1/advertisements', {
      method: 'POST',
      body: postData,
    }).then(response => {
      if (response.status === 201) {
        resetForm();
        props.onCreateAdvertisement();
      } else {
        response.json().then(body => {
          setErrors(body.errors[0].meta.messages);
        });
      }
    });
  }

  const PhotoUploader = (props) => {
    return (
      <div className={classes.photoUploader}>
        <input
          accept="image/*"
          className={classes.input}
          id="contained-button-file"
          type="file"
          onChange={props.onChange}
        />
      </div>
    );
  }

  return(
    <Dialog open={props.open} onClose={handleClose} fullWidth={true} maxWidth="sm">
      <DialogTitle>New Advertisement</DialogTitle>
      <DialogContent>
        {
          Object.keys(errors).length > 0 &&
          Object.keys(errors).map(attribute => (
          <Alert severity="error">{attribute}: {errors[attribute]}</Alert>
          ))
        }
        <form>
          <div>
            <TextField
              id="title"
              label="Title"
              className={classes.wide}
              value={title}
              onChange={(e) => setTitle(e.target.value)}
            />
          </div>
          <Grid container>
            <div>
              <FormControl className={classes.formControl} >
                <InputLabel id="category">Category</InputLabel>
                <Select
                  labelId="category"
                  id="category"
                  value={category}
                  onChange={(e) => setCategory(e.target.value)}
                >
                  <MenuItem value="cats">Cats</MenuItem>
                  <MenuItem value="dogs">Dogs</MenuItem>
                  <MenuItem value="hamsters">Hamsters</MenuItem>
                  <MenuItem value="snakes">Snakes</MenuItem>
                </Select>
              </FormControl>
            </div>

            <div>
              <TextField
                id="price"
                label="Price"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
              />
            </div>
          </Grid>
          <div>
            <TextField
              id="description"
              label="Description"
              className={classes.wide}
              multiline
              rows={4}
              variant="outlined"
              value={description}
              onChange={(e) => setDescription(e.target.value)}
            />
          </div>
          <div>
            <TextField
              id="address"
              label="Address"
              className={classes.wide}
              value={address}
              onChange={(e) => setAddress(e.target.value)}
            />
          </div>
          <div>
            <InputMask
              mask="+9 (999) 999-99-99"
              value={phone}
              onChange={(e) => setPhone(e.target.value)}
            >
              <TextField
                id="phone"
                label="Phone"
                className={classes.wide}
              />
            </InputMask>
          </div>
          <div className={classes.photos}>
            {
              photoInputs.map((input) => input)
            }
          </div>
          {
            photoInputs.length < 4 &&
            <Button onClick={addPhotoInput} color="primary">Add Photo</Button>
          }
        </form>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleSubmit} color="primary">Submit</Button>
      </DialogActions>
    </Dialog>
  )
}
