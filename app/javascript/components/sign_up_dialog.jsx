import React from 'react';
import { useState } from 'react';
import InputMask from "react-input-mask";
import { makeStyles } from '@material-ui/core/styles';
import {
  Button,
  DialogActions,
  Dialog,
  DialogTitle,
  DialogContent,
  TextField,
} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';

const useStyles = makeStyles((theme) => ({
  content: {
    display: 'flex',
    justifyContent: 'center',
  },
  input: {
    minWidth: 340,
  }
}));

export default function SignUpDialog(props) {
  const classes = useStyles();

  const [errors, setErrors] = useState({});
  const [email, setEmail] = useState('test@example.com');
  const [password, setPassword] = useState('password');
  const [username, setUsername] = useState('test');
  const [address, setAddress] = useState('as;lfdkjwa');
  const [phone, setPhone] = useState('q;wlkerjwelk');

  const handleSignUp = () => {
    const postData = { email, password, username, address, phone };

    fetch('/api/v1/users', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(postData),
    })
    .then(response => {
      if (response.status === 201) {
        props.onSignUp();
      } else {
        response.json().then(body => {
          setErrors(body.errors[0].meta.messages);
        });
      }
    });
  }

  return(
    <Dialog open={props.open} onClose={props.onClose}>
      <DialogTitle>Sign Up</DialogTitle>
      <DialogContent>
        {
          Object.keys(errors).length > 0 &&
          Object.keys(errors).map(attribute => (
          <Alert severity="error">{attribute}: {errors[attribute]}</Alert>
          ))
        }
        <form>
          <div>
            <TextField
              id="email"
              label="Email"
              className={classes.input}
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div>
            <TextField
              id="password"
              label="Password"
              type="password"
              className={classes.input}
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <div>
            <TextField
              id="username"
              label="Username"
              className={classes.input}
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
          </div>
          <div>
            <TextField
              id="address"
              label="Address"
              className={classes.input}
              value={address}
              onChange={(e) => setAddress(e.target.value)}
            />
          </div>
          <div>
            <InputMask
              mask="+9 (999) 999-99-99"
              value={phone}
              onChange={(e) => setPhone(e.target.value)}
            >
              <TextField
                id="phone"
                label="Phone"
                className={classes.input}
              />
            </InputMask>
          </div>
        </form>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleSignUp} color="primary">Sign Up</Button>
      </DialogActions>
    </Dialog>
  )
}
