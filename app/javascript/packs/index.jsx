import React from 'react';
import ReactDOM from 'react-dom';
import { useEffect, useState } from 'react';
import {
  Container,
  Snackbar,
} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';

import AdsBoard from '../components/ads/ads_board';
import CreateAdvertisementDialog from '../components/create_advertisement_dialog';
import Header from '../components/header';
import LogInDialog from '../components/log_in_dialog';
import SignUpDialog from '../components/sign_up_dialog';

const App = () => {
  const [message, setMessage] = useState(null);
  const [currentUser, setCurrentUser] = useState(null);
  const [createAdvertisementDialogOpen, setCreateAdvertisementDialogOpen] = useState(false);
  const [loginDialogOpen, setLoginDialogOpen] = useState(false);
  const [signUpDialogOpen, setSignUpDialogOpen] = useState(false);
  const [hasLastAds, setHasLastAds] = useState(false);

  const showMessage = (text) => {
    setMessage(text);
  }

  const hideMessage = () => {
    setMessage(null);
  }

  const handleNewAdvertisement = () => {
    setHasLastAds(false);
    setCreateAdvertisementDialogOpen(false);
  }

  const handleLogIn = (user) => {
    setCurrentUser(user);
    setHasLastAds(false);
    setLoginDialogOpen(false);
  }

  const handleSignUp = () => {
    setSignUpDialogOpen(false);
    showMessage("Signed up successfully!");
  }

  useEffect(() => {
    fetch('/api/v1/sessions/current')
    .then(response => {
      if (response.status === 200) {
        response.json().then(body => setCurrentUser(body.data));
      }
    });
  }, []);

  return(
    <React.Fragment>
      <Container maxWidth="lg">
        <Header
          currentUser={currentUser}
          onCreateAdvertisementClick={() => setCreateAdvertisementDialogOpen(true)}
          onLogInClick={() => setLoginDialogOpen(true)}
          onSignUpClick={() => setSignUpDialogOpen(true)}
        />
      </Container>

      <Container maxWidth="lg">
        <AdsBoard hasLastAds={hasLastAds} onAdsLoaded={() => setHasLastAds(true)}/>
      </Container>

      <LogInDialog
        open={loginDialogOpen}
        onClose={() => setLoginDialogOpen(false)}
        onLogIn={handleLogIn}
      />

      <SignUpDialog
        open={signUpDialogOpen}
        onClose={() => setSignUpDialogOpen(false)}
        onSignUp={handleSignUp}
      />

      {
        currentUser &&
        (
          <CreateAdvertisementDialog
            open={createAdvertisementDialogOpen}
            currentUser={currentUser}
            onClose={() => setCreateAdvertisementDialogOpen(false)}
            onCreateAdvertisement={handleNewAdvertisement}
          />
        )
      }

      <Snackbar open={message != null} autoHideDuration={6000} onClose={() => hideMessage}>
        <Alert onClose={hideMessage} severity="success">
          {message}
        </Alert>
      </Snackbar>
    </React.Fragment>
  )
}

document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(
    <App />,
    document.body.appendChild(document.createElement('div')),
  );
});
