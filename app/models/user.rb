class User < ApplicationRecord
  has_many :advertisements, dependent: :destroy

  has_secure_password :password, validations: false
end
