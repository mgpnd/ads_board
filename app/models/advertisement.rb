class Advertisement < ApplicationRecord
  CATEGORIES = %i[cats dogs hamsters snakes]

  enum category: CATEGORIES

  belongs_to :user

  has_many_attached :images
end
