module Api
  module V1
    class CurrentUserSerializer
      include JSONAPI::Serializer

      set_id :username

      attributes :email, :address, :phone
    end
  end
end
