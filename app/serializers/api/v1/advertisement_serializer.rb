module Api
  module V1
    class AdvertisementSerializer
      include JSONAPI::Serializer

      set_id :uid

      attributes :title, :category, :description, :price, :created_at

      attribute :address, if: -> (_, params) { params[:with_private] }
      attribute :phone,   if: -> (_, params) { params[:with_private] }

      attribute :photos do |advertisement, _|
        advertisement.images.map do |image|
          Rails.application.routes.url_helpers.rails_blob_path(image, only_path: true)
        end
      end
    end
  end
end
