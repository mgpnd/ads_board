module Api
  class ApiController < ActionController::API
    include Authenticatable

    def error(status, code)
      render json: { errors: [{ code: code }] },
             status: status
    end

    def present(entity, with:, status: 200, **params)
      serializer = with.new(entity, params: params)
      render json: serializer.serializable_hash, status: status
    end

    def validation_errors(errors)
      render json: {
        errors: [
          {
            code: 'validation.invalid',
            meta: { messages: errors }
          }
        ]
      }, status: 422
    end
  end
end
