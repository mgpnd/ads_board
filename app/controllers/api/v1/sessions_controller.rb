module Api
  module V1
    class SessionsController < ApiController
      include AdsBoard::Deps[
        authenticate_user: 'users.authenticate'
      ]

      def current
        if current_user
          present current_user, with: CurrentUserSerializer
        else
          error 401, 'sessions.not_authenticated'
        end
      end

      def create
        case authenticate_user.(params[:email], params[:password])
        in Failure(:not_authenticated)
          error 401, 'sessions.invalid_email_or_password'
        in Success(user)
          sign_in_user(user)
          present user, with: CurrentUserSerializer, status: 201
        end
      end

      def destroy
        sign_out_user
        status :no_content
      end
    end
  end
end
