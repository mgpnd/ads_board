module Api
  module V1
    class AdvertisementsController < ApiController
      include AdsBoard::Deps[
        search: 'advertisements.search',
        create_advertisement: 'advertisements.create'
      ]

      require_authentication_for :create

      def index
        case search.(params.permit!.to_h)
        in Success(advertisements)
          present advertisements,
                  with: AdvertisementSerializer,
                  with_private: current_user.present?
        end
      end

      def create
        case create_advertisement.(current_user, params.permit!.to_h)
        in Failure[:validation, errors]
          validation_errors errors
        in Success(advertisement)
          present advertisement,
                  with: AdvertisementSerializer,
                  with_private: true,
                  status: 201
        end
      end
    end
  end
end
