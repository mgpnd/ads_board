module Api
  module V1
    class UsersController < ApiController
      include AdsBoard::Deps[
        create_user: 'users.create',
      ]

      def create
        case create_user.(params.permit!.to_h)
        in Failure[:validation, errors]
          validation_errors errors
        in Success(user)
          present user, with: CurrentUserSerializer, status: 201
        end
      end
    end
  end
end
