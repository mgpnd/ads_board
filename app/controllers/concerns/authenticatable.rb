module Authenticatable
  extend ActiveSupport::Concern

  included do
    before_action :current_user
  end

  class_methods do
    def require_authentication_for(*actions)
      before_action :force_authentication, only: actions
    end
  end

  protected

  def current_user
    @current_user ||= authenticate
  end

  def sign_in_user(user)
    session[:current_user_id] = user.id
    session[:last_request_at] = Time.now.utc.iso8601
  end

  def sign_out_user
    session.delete(:current_user_id, :last_request_at)
  end

  private

  def authenticate
    return nil if session[:current_user_id].nil?
    return nil if session[:last_request_at].nil?
    return nil if (Time.now.utc - Time.parse(session[:last_request_at])) > SESSION_TIMEOUT

    user = User.find_by(id: session[:current_user_id])
    sign_in_user(user) if user
    user
  end

  def force_authentication
    error 401, 'authentication.unauthorized' unless current_user
  end
end
