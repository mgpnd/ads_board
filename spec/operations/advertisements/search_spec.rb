require 'rails_helper'

RSpec.describe Advertisements::Search do
  subject do
    described_class.new.call(params)
  end

  context 'when no params provided' do
    let!(:advertisements) { FactoryBot.create_list(:advertisement, 3) }

    let(:params) { { } }

    it 'returns all advertisements' do
      expect(subject.value!.map(&:id)).to eq(advertisements.map(&:id))
    end
  end

  context 'when filtered by category' do
    let!(:matching_ads) { FactoryBot.create_list(:advertisement, 3, category: :cats) }
    let!(:other_ads) { FactoryBot.create_list(:advertisement, 2, category: :dogs) }

    let(:params) { { category: 'cats' } }

    it 'returns only ads for requested category' do
      expect(subject.value!.map(&:category).uniq).to eq(['cats'])
    end
  end

  context 'when sorted by creation date' do
    let!(:advertisements) do
      3.times.map do |index|
        FactoryBot.create(:advertisement, title: index, created_at: index.days.ago)
      end
    end

    let(:params) { { sort_by: 'created_at' } }

    it 'returns advertisements sorted by creation date' do
      expect(subject.value!.map(&:title)).to eq(%w[2 1 0])
    end
  end

  context 'when sorted by price' do
    let!(:advertisements) do
      3.times.map do |index|
        FactoryBot.create(:advertisement, price: 50 - index)
      end
    end

    let(:params) { { sort_by: 'price' } }

    it 'returns advertisements sorted by price' do
      expect(subject.value!.map(&:price)).to eq([48, 49, 50].map { |x| BigDecimal(x) })
    end
  end
end
