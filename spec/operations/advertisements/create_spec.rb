require 'rails_helper'

RSpec.describe Advertisements::Create do
  let(:current_user) { FactoryBot.create(:user) }

  subject do
    described_class.new.call(current_user, params)
  end

  before do
    allow(Advertisement).to receive(:create!).and_return(Advertisement.new)
  end

  context 'when all validations passed' do
    let(:params) do
      {
        title: FFaker::CheesyLingo.title,
        category: Advertisement::CATEGORIES.sample.to_s,
        description: FFaker::CheesyLingo.sentence,
        price: 400
      }
    end

    it 'creates new advertisement' do
      expect(subject).to be_success
      expect(Advertisement).to have_received(:create!).with(hash_including(title: params[:title]))
    end

    it 'assigns current user to the new advertisement' do
      expect(subject).to be_success
      expect(Advertisement).to have_received(:create!).with(hash_including(user: current_user))
    end

    it 'assigns uid to the new advertisement' do
      expect(subject).to be_success
      expect(Advertisement).to have_received(:create!).with(hash_including(uid: kind_of(String)))
    end
  end

  context 'when required attribute missing' do
    let(:params) { { title: FFaker::CheesyLingo.title } }

    it { is_expected.to be_failure }
  end
end
