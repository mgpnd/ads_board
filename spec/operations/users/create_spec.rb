require 'rails_helper'

RSpec.describe Users::Create do
  let(:params) do
    {
      email: 'test@example.com',
      password: 'password',
      username: 'username'
    }
  end

  subject do
    described_class.new.call(params)
  end

  before do
    allow(User).to receive(:create!)
  end

  context 'when all validations passed' do
    it 'creates new user' do
      expect(subject).to be_success
      expect(User).to have_received(:create!).with(hash_including(email: params[:email]))
    end
  end

  context 'when required attribute is missing' do
    let(:params) { { email: 'test@example.com' } }

    it { is_expected.to be_failure }
  end

  context 'when email is already taken' do
    let!(:user) { FactoryBot.create(:user, email: params[:email]) }

    it { is_expected.to be_failure }
  end
end
