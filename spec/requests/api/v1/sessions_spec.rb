require 'rails_helper'

RSpec.describe '/api/v1/sessions', type: :request do
  describe 'POST /' do
    let(:user) { FactoryBot.create(:user) }

    subject do
      post '/api/v1/sessions',
           params: { email: user.email, password: password }
    end

    context 'when credentials are valid' do
      let(:password) { 'Password123' }

      it 'renders corresponding user' do
        subject
        expect_status(201)
        expect_json('data', id: user.username)
        expect_json('data.attributes', email: user.email)
      end
    end

    context 'when credentials are invalid' do
      let(:password) { 'invalid_password' }

      it 'render 401 error' do
        subject
        expect_status(401)
        expect(json_body[:errors][0][:code]).to eq('sessions.invalid_email_or_password')
      end
    end
  end
end
