require 'rails_helper'

RSpec.describe '/api/v1/advertisements', type: :request do
  describe 'GET /' do
    let!(:advertisements) { FactoryBot.create_list(:advertisement, 3) }

    subject do
      get '/api/v1/advertisements'
    end

    context 'when user is not logged in' do
      it 'renders advertisements list without contacts and phones' do
        subject
        expect_status(:ok)

        expect(
          json_body[:data].map { |x| x[:id] }.sort
        ).to eq(advertisements.map(&:uid).sort)

        expect(json_body[:data].map { |x| x[:attributes][:address] }.uniq).to eq([nil])
        expect(json_body[:data].map { |x| x[:attributes][:phone] }.uniq).to eq([nil])
      end
    end

    context 'when user is logged in' do
      before do
        login_user
      end

      it 'renders advertisements list with contacts and phones' do
        subject
        expect_status(:ok)

        expect(
          json_body[:data].map { |x| x[:id] }.sort
        ).to eq(advertisements.map(&:uid).sort)

        expect(
          json_body[:data].map { |x| x[:attributes][:address] }.sort
        ).to eq(advertisements.map(&:address).sort)

        expect(
          json_body[:data].map { |x| x[:attributes][:phone] }.sort
        ).to eq(advertisements.map(&:phone).sort)
      end
    end
  end
end
