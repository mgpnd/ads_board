RSpec::Matchers.define :be_success do
  match { |r| r.success? }

  failure_message do |actual|
    "expected #{actual} to be Success"
  end

  failure_message_when_negated do |actual|
    "expected #{actual} not to be Success"
  end
end

RSpec::Matchers.define :be_failure do
  match { |r| r.failure? }

  failure_message do |actual|
    "expected #{actual} to be Failure"
  end

  failure_message_when_negated do |actual|
    "expected #{actual} not to be Failure"
  end
end
