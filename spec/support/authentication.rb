module Authentication
  def login_user(user = nil)
    user ||= FactoryBot.create(:user)
    allow_any_instance_of(Authenticatable).to receive(:current_user).and_return(user)
  end
end
