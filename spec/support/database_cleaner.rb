RSpec.configure do |config|
  config.before(:suite) do
    DatabaseCleaner.clean_with(:truncation, except: ['spatial_ref_sys'])
  end

  config.around :each do |example|
    DatabaseCleaner.strategy = :transaction

    DatabaseCleaner.cleaning do
      example.run
    end
  end
end
