FactoryBot.define do
  factory :advertisement do
    uid { AdsBoard::Container['services.generate_uid'].() }
    title { FFaker::CheesyLingo.title }
    category { Advertisement::CATEGORIES.sample }
    description { FFaker::CheesyLingo.sentence }
    price { rand(99..999) }

    user

    address { user&.address }
    phone { user&.phone }
  end
end
