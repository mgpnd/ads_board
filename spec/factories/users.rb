FactoryBot.define do
  factory :user do
    email { FFaker::Internet.email }
    password { 'Password123' }
    username { FFaker::Internet.user_name }
    address { FFaker::AddressAU.full_address }
    phone { FFaker::PhoneNumber.phone_number }
  end
end
