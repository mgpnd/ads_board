class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.text :email, null: false, index: true, unique: true
      t.text :password_digest, null: false
      t.text :username, null: false, index: true, unique: true
      t.text :address
      t.text :phone

      t.timestamps
    end
  end
end
