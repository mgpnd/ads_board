class CreateAdvertisements < ActiveRecord::Migration[6.0]
  def change
    create_table :advertisements do |t|
      t.text :uid, null: false
      t.text :title, null: false
      t.integer :category, null: false
      t.text :description
      t.decimal :price
      t.text :address
      t.text :phone

      t.timestamps
    end
  end
end
