class AddUserToAdvertisements < ActiveRecord::Migration[6.0]
  def change
    add_reference :advertisements, :user, index: true, foreign_key: true
  end
end
