# frozen_string_literal: true

Rails.application.routes.draw do
  root to: 'app#index'

  match 'app/*path', to: 'app#index', via: :get

  namespace 'api' do
    namespace 'v1' do
      resources :advertisements, only: %i[index create]
      resources :users, only: %i[create]

      resources :sessions, only: %i[create destroy] do
        get :current, on: :collection
      end
    end
  end
end
