module Contracts
  class ApplicationContract < Dry::Validation::Contract
    config.messages.backend = :i18n

    def call(params)
      case super.to_monad
      in Failure(result)
        Failure[:validation, result.errors.to_h]
      in Success(result)
        Success(result.to_h)
      end
    end
  end
end
