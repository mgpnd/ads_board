require_relative '../application_contract'

module Contracts
  module Advertisements
    class New < ApplicationContract
      params do
        required(:title).filled(:string)
        required(:category).filled(:string, included_in?: Advertisement::CATEGORIES.map(&:to_s))
        required(:description).filled(:string)
        required(:price).filled(:decimal, gt?: 0)
        optional(:address).filled(:string)
        optional(:phone).filled(:string)
        optional(:photos)
      end
    end
  end
end
