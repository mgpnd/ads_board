require_relative '../application_contract'

module Contracts
  module Users
    class New < ApplicationContract
      params do
        required(:email).filled(:string, format?: URI::MailTo::EMAIL_REGEXP)
        required(:password).filled(:string)
        required(:username).filled(:string, max_size?: 32)
        optional(:address).filled(:string)
        optional(:phone).filled(:string)
      end

      rule(:email) do
        key.failure(:already_taken) if User.where(email: values[:email]).exists?
      end

      rule(:username) do
        key.failure(:already_taken) if User.where(username: values[:username]).exists?
      end
    end
  end
end
