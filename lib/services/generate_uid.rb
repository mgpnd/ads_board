module Services
  class GenerateUid
    def call
      (0...2).map { rand(65..90).chr }.join + format(
        '%06i', SecureRandom.random_number(999_999)
      ) + (0...2).map { rand(65..90).chr }.join
    end
  end
end
