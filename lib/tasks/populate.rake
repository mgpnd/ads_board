def create_user
  User.create(
    email: FFaker::Internet.email,
    password: 'Password123',
    username: FFaker::Internet.user_name
  )
end

namespace :populate do
  desc 'Create sample advertisements'
  task ads: :environment do
    print 'Populating advertisements...'
    20.times do
      user = create_user
      Advertisement.create(
        uid: AdsBoard::Container['services.generate_uid'].(),
        title: FFaker::CheesyLingo.title,
        category: Advertisement::CATEGORIES.sample,
        description: FFaker::CheesyLingo.sentence,
        price: rand(99..999),
        user: user,
        address: user.address,
        phone: user.phone
      )
    end
    puts 'Done!'
  end

  desc 'Create test user'
  task user: :environment do
    User.create(
      email: 'test@example.com',
      password: 'Password123',
      username: 'test'
    )
  end
end
