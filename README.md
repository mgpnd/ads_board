# ADS Board

## Installation

Install all dependencies with bundler
```bash
bundle install
```

Install all JavaScript dependencies
```bash
yarn install
```

Copy `.env.sample` file to `.env` and set up values for your environment
```bash
cp .env.sample .env
```

Setup database
```bash
bin/rails db:create
```

Load schema to the database
```bash
bin/rails db:schema:load
```

Create test user with email **test@example.com** and password **Password123**
```bash
bin/rails populate:user
```

Create sample advertisements
```bash
bin/rails populate:ads
```

Run Rails server
```bash
bin/rails s
```

Run Webpack dev server
```bash
bin/webpack-dev-server
```
